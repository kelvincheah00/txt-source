# 含有日文的章節段落

[TOC]

## 00000_決意の乳児期：兄妹転生編/00050_嬌

- 『♪　せっせっよせーのよいいよい　♪』


## 00000_決意の乳児期：兄妹転生編/00060_一万尺♪

- 「♪　せっせーせーのよいよいよい　♪」


## 00000_決意の乳児期：兄妹転生編/00070_於是，他的人生正式開始

- いくら赤醬でも、兄桑は兄桑なのに。
- わたしが愛する、わたしだけが愛する、究極無比にして天下無雙、最大最強にして萬夫不當の、宇宙一カッコいい兄桑なのに。
- ２０年ぶりくらい？
- ともかくともかく、話しかけてくれたんです、兄桑が！
- いいですね、サプライズ！とってもステキです！
- また次回、新しい人生でお會いしましょう！


## 00010_黄金の少年期：才能胎動編/00010_運命の日

- ぐえ～⋯⋯ぺっ。可惡⋯⋯（請腦補成嘔吐的聲音）
- ゴツン！
- 「わー！うわー！ひろーい！」
- ぴくっ。（皮褲，擬聲詞）


## 00010_黄金の少年期：才能胎動編/00020_師匠

- 「もぐもぐ、もぐもぐ、もぐもぐもぐもぐ」（咀嚼聲）


## 00010_黄金の少年期：才能胎動編/00030_修行風景with少女

- 「えいっ」
- 「ふぉくじひゅうにふるほわほほわふぁいひゃろ（吃飯的時候不認為會來吧）！」（嘴中被塞了異物，口齒不清，吾就不翻了，太麻煩）
- 「はあっはっは！！預想以上的實踐嗎！？瑞秋桑有當教官的經驗嗎？」
- すでに充分混ざってただろというツッコミはさておくとして、菲兒の弟子入りは、也沒有取得監護人波斯福德先生的許可，最開始是猶豫不決。（這是什麼鬼，表達不清啊。）
- 肩頭上鳥的嘴也つんつん的啄「又來了好癢啊！」菲兒笑著。


## 00010_黄金の少年期：才能胎動編/00040_和可愛的青梅竹馬與隱性巨乳的精靈師傅幸福地混浴後陪睡回 

- 「ッッッ！？！？！？」
- わしゃわしゃわしゃわしゃわしゃ。
- 「力強すぎない？」
- わしゃわしゃわしゃわしゃわしゃ。
- 「⋯⋯なんか無口じゃない？」


## 00010_黄金の少年期：才能胎動編/00050_翌日

- 然後，２人都きゃっきゃ（拼音是kiyakiya應該和嘎嘎一樣的= =）地笑著。
- 「秘密（ないしょっ）」
- 「姆⋯⋯不是我的錯姆（もん讀作Mong）。基君的錯姆（基君のせいだもん）」
- 『哼～？哼哼（ふーん？　ふんふん）』
- 摸了摸頭。很優秀很優秀（なでなでする。えらいえらい男主在安慰菲兒？）


## 00010_黄金の少年期：才能胎動編/00060_♪俺たちゃ盗賊！♪

- ズン！ズン！ズン！ズン（Zien）！
- 像這樣玩耍著做這事，故意犯些錯誤，演出真實感是理所當然的（こうしてお遊びにして、あえてミスを誘發して、マジっぽさを演出してるってわけさ）
- 既不是瘋子也沒有嗑藥的症狀，很普通的人類，在客觀的計算的基礎上，冷靜地做出這種看似瘋狂的事。（狂人ってわけでもヤク中ってわけでもない、ごくごく普通の人間が、純然真っ當な計算のもとに、こういう一見トチ狂ったことを、平然とやっちまうのさ。要死要死，應該說的是一個）
- 「piigiiiiiiピィギィィイィイイイイイイイイイィィッッ！！！」
- 「♪　俺たちゃ盜賊！泣く子も奪う！♪」


## 00010_黄金の少年期：才能胎動編/00070_廃砦の逃走劇

- ⋯⋯鼻歌にはいい思い出がない。
- 「♪　ランランランランラ⋯⋯　♪」
- そんな馬鹿な。
- ⋯⋯ダメだな。こんなときにトラウマにやられてるようじゃ。
- 「基君、どうする？」
- 「隠れて逃げるしかないかな？」


## 00010_黄金の少年期：才能胎動編/00080_絶跡の虚穴

- 「切⋯⋯臭小鬼些。東跑西跑東跑西跑（ちょこまかちょこまかと）⋯⋯」
- 貴族那些傢伙絶望的臉浮現在眼前了だぜ（Dazei混混語氣，實在翻不出來）
- 「沒用呢，木大木大（無駄だね。無駄無駄）。聰明如你也能注意到吧？　我一直都知道你們的藏身之處ア。再加上我的【絶跡的虛穴】，無論跑多遠都是沒用的」
- ⋯⋯利用這一點，你對部下誇大了自己作為精靈術師的實力。『加工』的時候使用哄小孩的把戲也是為了提高部下對自己的忠誠度吧？（自分への求心力を高めるためだったんだろ。翻譯成向心力感覺有怪怪的）」
- 咔咔咔（メキメキメキ）──發出了不好的聲音。那聲音發生源是──


## 00010_黄金の少年期：才能胎動編/00090_時隔七年の生存闘争

- ギィイィイイインッ！！


## 00010_黄金の少年期：才能胎動編/00110_結末

- 特別的是，（我們）還未到入學王立精靈術學院的時候。（とりわけ、王立精靈術學院に屆くまで、そう時間はかからなかった。）


## 00020_黄金の少年期：神童集結編/00020_只是做的話，誰也不會誇獎的世界

- 「────っきゃあああああああああああああああああああああああああああああああああああああああああああああああああっっっっ！！！！！」


## 00020_黄金の少年期：神童集結編/00040_盗賊潰しと天才王子

- （注：上面這些「我」都是用的男性自稱──オレ=俺）
- 盯──────（じーっと）
- 被６連斬的學院生，伴隨著出局，從場上緩緩倒下（その場に崩れ落ちた／作者應該是在表述被砍成一塊塊的岩石巨人崩塌的樣子）
- 也就是說和我一樣，選拔組。（スカウト組）
- 因為那裡，就像是從搖動的熱氣流中，人類出現了。（搖動的熱氣流：大概就是夏天熱氣騰騰導致空氣扭曲的樣子／原文是まるで陽炎のように揺らめいた）
- 滋恩──────（ッッッズン）！！！！！！


## 00020_黄金の少年期：神童集結編/00050_選ばれしクラスメイト

- 嘛、那應該就是他與露比合不來的原因了（まあそりゃあ、露比と折り合いが惡くもなろうというものだ。這句不懂）
- ⋯⋯思ったよりずいぶんと腰が低くて毒氣を拔かれたが。（這句什麼意思，真的不懂）
- 「嘿 你們這群小鬼頭（原文是學生）老身可是比你們年長了４００多歳じゃぞ！雖然具體的數字早就忘了吶！不管怎樣 你們對年上者的老身都要抱有敬意！要抱有４００年的敬意！」
- 「うむうむ」地點了點頭。
- 「真正的教學從明天開始，今日就當是所謂的オリエンテーリング（這東西不知道是啥）
- 你們各自，雖然大體上都已經了解了，但是我會再次說明這所學院的機制與體系──そう」


## 00020_黄金の少年期：神童集結編/00060_蟲毒

- 銀髮的學院長似乎很高興，「嘻嘻嘻（ひひひ=hihihi）」地笑著。
- 『自己在這世上無法走到頂點』（自分では、この世界でやっていくことはできない）」
- 「那麼，今天就到這。嘿（よっと Yooto：跳下去的時候說的語氣詞）」
- 「『『誒誒誒誒誒誒誒誒誒誒誒誒誒誒──────っっっ！？！？』』」
- 嘛啊，沒多久就迅速引退了。現在想來，那是因為瑪德琳的肚子裡懷了您吧」（注：學院長對男主用尊稱實在是翻著難受，她叫男主おぬし=お主）
- 「那⋯⋯！不！並不！不是說老身！話說你（おぬし）當初不是追求而是糾纏吧！」（叫男主おぬし【您】叫丈夫也是おぬし【老公】，是說不對勁。順便提一句，御主也是おぬし，雖然我不玩這遊戲）


## 00020_黄金の少年期：神童集結編/00070_級位戦

- 「眼睛挺好的呢。──背上的制服裂開時，看到了發紅的鱗片」（赤茶けた鱗。想翻成紅茶）
- 而且就算只有１秒，也要讓自己活得更久。（そして１秒でも、命をながらえるためにじゃ）』


## 00020_黄金の少年期：神童集結編/00100_王の眼

- 【原文：一發でも攻擊を當てようと、スピードを上げ、フェイントを入れ、ほんの少しでも裏をかくべく工夫を凝らし】
- 這是堂堂正正的宣戰佈告啊啊────っっ！！！！』


## 00020_黄金の少年期：神童集結編/00130_神童ＶＳ神童・上

- 『防、防禦啦ＡＡＡ────っっ！！！至今徹底迴避的溫莎２級！第一次！是的、第一次！！和對戰對手接觸了ＡＡＡ────っっ！！！』


## 00020_黄金の少年期：神童集結編/00140_神童ＶＳ神童・中

- 『直⋯⋯直擊────っっ！！！激烈地撞在了比賽場的牆壁上！但是⋯⋯還站著！！站起來了！！利巴２級、避免了出局！！』


## 00020_黄金の少年期：神童集結編/00160_宴の後

- ♪　せっせっせーのよいよいよい　♪


## 00030_黄金の少年期：貴族決戦編/00030_臥人館の誘い

- 「』％』＆＄＃％＄』＆＆＃＄っっっ！？！？！？」


## 00030_黄金の少年期：貴族決戦編/00070_臥人館の謎その１【解答編】

- 基克洛普斯サイクロプスの『サ』
- 人類ニンゲンの『ニ』
- 愛之間：奧克オークの首
- 壊死之間：人類ニンゲンの首
- よみ　＋　ド　＝　よどみ（停滯）


## 00030_黄金の少年期：貴族決戦編/00080_臥人館の謎その２【出題編】

- 【原文：視力落ちたんじゃないか、これ⋯⋯⋯】


## 00030_黄金の少年期：貴族決戦編/00100_臥人館の謎その３【出題編】

- ・那麼，什麼提示也沒有能處理那樣的苦行嗎？【原文：そんな難行を、さて、何の道標もなくこなせるものだろうか？】


## 00030_黄金の少年期：貴族決戦編/00110_臥人館の謎その３【解答編】

- みゅーたんとごぶりん。
- ばんしーりりー。
- されこうべ⋯
- だいがるーだ。
- ・だがな・まえがある。つまりだれかがなづけた。


## 00030_黄金の少年期：貴族決戦編/00150_最初的弟子

- 對戀慕是弱點這句話，妾身認為被戀慕才是弱點。【原文：惚れた弱みという言葉があるが、儂は惚れられた弱みというやつも充分にあると思う】


## 00030_黄金の少年期：貴族決戦編/00170_少年少女

- 明明還沒到那樣的關係就想著之後的事情了（諺語「捕らぬ狸の皮算用もするしさあ」）


## 00030_黄金の少年期：貴族決戦編/00200_開会前

- 另一方面，【武闘紳士】布拉德利・莫德里奇九段和巴斯蒂德九段年的齡相差無幾。得意技是近身精靈格闘術『マナーズ・魔法』，優雅與力量兼備，在貴族和平民之中都特別受歡迎的一個精靈術師。


## 00030_黄金の少年期：貴族決戦編/00210_王太子と第三王子

- （原文：「うへー。歳食った高文じゃん」
- 「高文のほうが若い巴斯蒂德九段なんだろうけどな」
- 「禮儀正しいねー」）


## 00030_黄金の少年期：貴族決戦編/00220_宣戦布告

- （原文：「⋯⋯ええ。戰うことにしか關心のない、野蠻な弟で。やはり血筋でしょうか」）


## 00030_黄金の少年期：貴族決戦編/00230_Her love even kills the god's scenario

- 就像【麥田怪圈】（原文：ミステリーサークル，你問我，我也不知道這是什麼鬼。）一樣的東西。
- 有各種各樣想法的貴族們的【Power Game】啦。（原文：パワーゲーム）
- よし、できた！
- それでは、改めまして！
- 余計なキャストを一掃するめくるめく後片付け！
- 學院鏖殺編、はじまりはじまりー☆


## 00030_黄金の少年期：貴族決戦編/00240_Welcome to Nightmare World ‐ Part１

- 「不管有多少老師，人手也還是不夠⋯⋯！師父在沙龍（原文：サロン，大廳之類的？）那邊！只要讓師父來幫忙對付這些怪物的話⋯⋯！」


## 00030_黄金の少年期：貴族決戦編/00260_The Rain of Dullahan

- （原文：──ギギギギギ⋯⋯⋯
- -刷拉刷拉──（原文：──ぶらーん──）
- ──ガシャンッ！
- ──ガシャンッ！
- ──ガシャンッ！
- ──ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャンッ！ガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャガシャ


## 00030_黄金の少年期：貴族決戦編/00270_Welcome to Nightmare World ‐ Part２ｂ

- （原文：本物なんだ、惡靈王とか名乗ったあの畢弗隆斯は！」）


## 00030_黄金の少年期：貴族決戦編/00290_Return of Nightmare

- 「♪　せっせっせーのよいよいよい　♪」


## 00030_黄金の少年期：貴族決戦編/00340_From Teacher To Students

- 「────哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦哦啦っっっxiaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa！！！！！！」


## 00030_黄金の少年期：貴族決戦編/00400_When the Blue Flame Blooms - Part４

- 「因為你好像沒察覺到所以我告訴你。你、真不像樣啊。語調也好舉止打扮也好太過頭了。洩露了自卑感。『ですわ』什麼的，只會在正式場合使用。差勁」
- 「什麼啊那個名字！！給我適可而止你的老公才會私通吧！！那種傢伙，稍微誘惑一下就屈服了！！」【原文：いい加減にしないと寢取るわよあんたの旦那】


## 00030_黄金の少年期：貴族決戦編/00430_The Chirp of Girl - Part３

- （原文：ネタにマジになんないでくださ～い！）


## 00030_黄金の少年期：貴族決戦編/00440_The Chirp of Girl - Part４

- 「哈哈、哈哈哈哈哈哈！！哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈───っっ！！！」


## 00030_黄金の少年期：貴族決戦編/00470_Lonely Strongest - Part１

- 輕飄飄～⋯⋯っと。
- 「滾開⋯⋯ッッッ！！！！」
- 「あれか⋯⋯⋯ぱい⋯⋯⋯た⋯⋯す！⋯⋯⋯⋯と⋯⋯⋯⋯んみ⋯⋯⋯⋯な⋯⋯⋯⋯に！」


## 00030_黄金の少年期：貴族決戦編/00480_Lonely Strongest - Part２

- 「啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊っっ！！！」
- 「咕⋯⋯嗚、啊啊啊啊啊啊啊啊啊啊啊啊啊啊っっ⋯⋯！！！」


## 00030_黄金の少年期：貴族決戦編/00550_Visibility of Last Moment

- 「─────Ａ───────ａａａａａａ───────ッッ！！！！」
- 「快逃啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊──────っっ！！！！」


## 00030_黄金の少年期：貴族決戦編/00560_Wrong Yearning

- 「啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊──────ッッッ！！！！！！！」


## 00030_黄金の少年期：貴族決戦編/00600_カタストロフ・ポイント ‐ Part２

- 『哼─嗯（ふーん）。這下雨天的真虧⋯⋯』
- 『是電視劇（ドラマ）的重播啦。還蠻舊的』
- アーロ倒下了，【試練迷宮】被解除，往返學院內外變得自由了。
- 「あぁあぁああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああッッッッ！！！！！！！！！！！！！！」（這擬聲詞不翻了，幾乎都是啊的意思）


## 00030_黄金の少年期：貴族決戦編/00610_カタストロフ・ポイント ‐ Part３

- 「傑克っっ！！！！」
- 「はい？（你是指？）」
- 「可以說是類似AI之類的東西了啦～。僅僅是依照生前記憶而行動的機器人了啦～。那便是我的精靈──＜命之所在的畢弗隆斯＞的精靈術，【死止ししの蝋燭】的力量。」


## 00030_黄金の少年期：貴族決戦編/00620_カタストロフ・ポイント ‐ Part４

- 「あぁあぁあああああああああああああああああああああああああああああああああああああああああああッッッ！！！！！！！」
- 拂曉之劍從手中跌落，隨著ズン的一聲！劍落入由白骨舖就而成的地面。
- 「うぁああおおおおおおおおああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああッッッ！！！！！」（男主的咆哮）
- 「ぁぁあぁぁぁぁぁあ！！！ぁぁぁああ！！！！ぁあぁあぁあああああああああああああああ！！！ァあアあああああぁアあああああああああああああああアアアアアアアアアあああああああああああああああああああああああああああああああああ！！！！！あぁあぁアアアアアぁアアアアアアアアアアアアアアアアアアアアアアアアアアアアああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアアあああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああ────────────────────っっっっっ！！！！！！！！！！！！！！！」
- ───ガンゴンガンガンギンゴンゴンガンッ！！！！


## 00040_因果の魔王期：門扉開啟之日/00020_第１話　異形なる侵略者

- 「然後告訴？（指代不明）吧。そして教えてやるがいい。──人們向天災彎腰的愚蠢」
- 「素直にお言いなさい」
- それでも、臣下の務めを粛々と果たす。
- 布萊迪亞內への侵入を阻む手段はもはやなく⋯⋯明日の朝日が昇る頃には、この城の旗は魔王のものに替えられているでしょう」
- 「やって、みろやァああああああああああッッ！」


## 00040_因果の魔王期：門扉開啟之日/00040_第３話　残骸たち

- ───ゴゴゴ───
- ───ゴゴゴゴゴ───


## 00040_因果の魔王期：門扉開啟之日/00050_第４話　那天的門扉

- 「シッ！」


## 00040_因果の魔王期：門扉開啟之日/00280_第２７話　為了打開那日的門 - Part３

- 讓人聯想起曾經的《パラボール》的巨大單眼低溜低溜地轉動，從球狀巨體像體毛一樣長出的槍和大砲射出的彈幕碾壓而來。


## 00040_因果の魔王期：門扉開啟之日/00550_第３４話　我將其否定！！！

- 「時間回溯───〈沙克斯・As・阿加雷斯〉！！」【葉：沙克斯・アズ・阿加雷斯】
