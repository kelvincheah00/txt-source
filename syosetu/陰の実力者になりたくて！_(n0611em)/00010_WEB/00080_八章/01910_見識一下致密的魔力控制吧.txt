  那是個白髮的帥哥。


  看起來就像是個很強的角色，不過到底是怎麼出現的呢？雖然看上去像是空間扭曲了，但是卻完全沒有感覺到魔力。

「我想你應該已經知道了吧、我就是莫德雷德」

  帥哥自稱是莫德雷德。

「——確有耳聞」

  並沒有說是聽誰說的，因為並沒有從任何人那裡聽過呢。

「作為參考可以問一下嗎。你是怎麼知道這個地方的？」

  我露出了意味深長的微笑等了片刻。

「——被過去引導而來」

「原來如此、過去嗎……」

  莫德雷德看了看芙蕾雅。

「確實可以說是過去的殘骸呢。真是可怜的女人……」

  芙蕾雅緊緊瞪著莫德雷德、是認識的人嗎？

「剛觀測到黑薔薇，如今又出現了過去的殘骸。這不是偶然。是你幹的嗎、暗影」

  若要問是不是我幹的的話，那確實是我的緣故。

  但是莫德雷德君看起來像是這個厲害的研究設施的重要人物。而且在雖然是手指和幽靈卻還是有兩個觀眾的情況下 ，這裡必須得尋求有帥氣的影之實力者感覺的具有深度的回答。

「終幕已然臨近——清算一切的時刻即將到來」

  深邃的聲音回響起來，我放出了魔力的波動。

  青紫色的魔力如同火焰一般搖曳的包裹著我的身體，那個風壓使得漆黑的長衣飄動起來。

  好帥。

「那個魔力……是真傢伙嗎」

  因魔力而引起的風吹動了莫德雷德的頭髮。

  這個可是相當困難的啊。


  如果只是一瞬的話還很簡單，若是要使之一直維持下去的話魔力的消費就變的十分大了。控制也變得非常困難。為了抑制魔力的消耗同時提高壓力，必須高速地重複壓縮與放出，想要維持一定的風壓必須使魔力保持均衡。


  我的風壓無論何時都是一致的。

  也就是說是完美的控制著，知道了這個事實莫德雷德應該也感到戰慄了吧。

「雖然魔力和密度確實驚人……但強弱可不光是靠魔力決定的啊」

  不，重點不是那裡啊。

  我是為了細致的製作風壓而控制了強弱。看啊、這精密的魔力控制。

「雖然在這裡做你的對手也沒關係、不過……清算之時貌似還早了點啊」

  莫德雷德這麼說著的同時、空間扭曲了起來。並且空間之中像是發出噪音一般出現了一道裂縫。

「下次就從正規的入口過來吧——不請自來的傢伙們啊」

  啊、這是還不能boss進行戰鬥的事件呢。

「這樣嗎……好吧」

  能感覺到從空間之中釋放出某種、與魔力完全不同的力量。

  果然與聖域那時感覺到的力量很相似——也就是說。

「I·am……Atomic」

  我抱著、只要將一切都吹飛就沒問題了、這樣的精神將其釋放了出來。







◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇◆◇







「這是……」

  莫德雷德不禁為之戰慄。


  在自己脫離之際、暗影所釋放出的一擊，給空間帶來了巨大的損傷。

  存放實驗體的膠囊倉盡數崩壞，空間之中開了大洞。如果再晚一瞬的話，整個空間都會被吞沒了吧。

  簡直就像是吞噬世界的黑洞一般，要修復恐怕得花些時間了吧。

「那傢伙的魔力足以影響空間嗎……不、那傢伙正是為此而準備了如此大量的魔力吧」

  只能這樣想了。

「也就是說、那傢伙打從一開始就知道。這個空間位於什麼場所，還有這個空間所具有的意義……」

  莫德雷德的臉上流下了冷汗。

「終幕已然臨近、清算一切的時刻即將到來」

  莫德雷德低聲念叨著暗影留下的話語。

  終幕已然臨近——確實如此。正因如此、教團才會開始行動。

  然後、清算一切之時——

「是打算要清算我等嗎」

  毫無疑問，暗影是知道的。不管是什麼全部都知道。

「只能提前計劃了、那傢伙……他知道『魔界』的存在」

  然後就在回過頭來的瞬間。

  啪嗒、一聲。

  有什麼東西落在了地板之上。

「什麼……？」

  那是鮮血。

  鮮紅的血液滴落在白色的地面之上擴散開來。


  這時、莫德雷德才剛剛注意到了右臂失去了知覺一事。他的右臂、從肩膀處起正以快要被切斷的狀態垂下著。

「暗影——竟然有這種程度嗎」

  莫德雷德用冰冷的眼神凝視著暗影消失的那片空間。
