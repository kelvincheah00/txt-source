８

隔天，雷肯去了租的房子。
進了圍欄後，小小的庭院被整理得非常乾淨，還以為搞錯了地方。

「阿，雷肯。早安。」
「嗚嗯。」
「雷肯的房間在最裡頭。」
「了解。」
「早餐已經吃了嗎？」
「啊啊。」
「那，我去做飯吃，你等一下喔。」
「啊啊，還有這個。」
「這是什麼？」
「購物的資金。」
「不需要喔。」
「總之先收下，需要的話就用吧。」
「那，就那麼辦吧。謝謝。」

喝著艾達泡的茶，等待艾達吃飯。
告訴她，諾瑪說隨時都能去，還有昨天得知了諾瑪的住處。

「阿啦，那吃完飯得換個衣服呢。」

雷肯完全搞不懂，為什麼去諾瑪那裡需要換衣服，但也沒打算問理由。

「妳那模樣，被襲擊的話不會很難行動嗎？還有，弓怎麼了？」
「你阿，去協助施療師，是打算戰鬥嗎？」

艾達雖然有帶著放了〈伊希亞之弓〉的〈箱〉，但野營道具等都放在家裡。仔細想想，這在這世界上是理所當然的。
艾達把玄關外側的門關上後，雷肯用〈移動〉把門閂扣上。

「等等，剛剛做了什麼？」
「扣上了門閂。」
「那個沉重的門閂？從門的外側？」
「啊啊。」
「我回來後要怎麼進去？」
「我來打開。」
「雷肯不在的話呢？」
「飛越進去吧。」
「做得到嗎！不，也不是做不到，但是為什麼進入自己的家，非得這麼悲哀，從圍牆跳進去？」
「只用小鎖就太粗心了吧。」
「把門閂打開來。」

結果，門閂只有在離開城鎮時才要扣上。


９

「你就是雷肯嗎。哎呀，真的好高大呢。好久沒遇到得讓我抬頭看的人了。然後你就是艾達對吧。請多指教。」

諾瑪是個身穿潔白樸素服裝的高個子女性。姿勢挺拔端正，有著男性美。沒有多餘動作的俐落舉止，給人女劍士的印象。不過，沒有武藝經驗是一目瞭然的，戰鬥力估計很低吧。

「諾瑪大人。有客人嗎。」
「啊啊，金格。希拉小姐介紹的兩位〈回復〉持有者來了。從今天開始，會在這間施療所協助一個月左右。我也會教導如何治療疾病和傷口，所以沒有薪水，也沒有學費。雷肯，艾達。這位老人是金格。負責這間施療所的一切幕後工作。」
「我是金格。請多指教。」
「啊啊。」
「他是雷肯。我是艾達。麻煩關照了。」

金格這老人，過去應該是上級士兵，也說不定是騎士。雖然有些瘦，但步伐之中毫無破綻，肌肉也依舊強韌。會故意讓部分動作顯得凌亂，但看得出來，很習慣有條理的動作。

是劍士，嗎。
只感覺得到少許魔力。不過，威武非比尋常，散發著長年置身於嚴苛戰鬥之人會有的，獨特氣質。

「那麼，我聽說雷肯有做過一點藥師的修行。就麻煩二位到庭院去吧。」

很出色的中庭。
寬廣而深的中庭，遍佈藥草。
這麼說來，這間房子也是，雖然老舊，但造得很漂亮，房間也多。作為施療所的房間，原本應該是接待室和等候室吧。

「雷肯，能舉出這庭院裡生長的藥草的名稱和效果嗎。我想知道你的知識有多少程度。」
「知道了。」

雷肯在庭院裡走來走去，告訴諾瑪自己知道的藥草的名稱和效果。
三人之後在施療所的等候室，喝了金格泡的茶。

───

「真厲害呢，雷肯。庭院裡生長的三分之二左右的藥草都知道。明明有很多稀有的藥草，還有通常在平地不會看到的藥草呢。而且，竟然連豆粒草、龍檜、地獄草都知道。」
「那三種毒草，希拉家的庭院都有。」
「誒？太危險了。」
「生長在希拉家的庭院的，幾乎都是毒草，所以沒有毒耐性的人打從一開始就進不去。這裡的庭院，混雜了藥草跟毒草。」

說著，雷肯注意到了某個事實。
傑利可有毒耐性。不會有錯。

（那隻猿猴，到底是何許人物？）

「哈哈哈。因為這樣就只有我跟金格進得去。不過，這麼一來，也能拜託雷肯採取和處理藥草。效用這方面的知識也相當了不起。拜希拉小姐為師，大概有多久了？」
「是從二月上旬開始的，所以有兩個月半吧。」
「嘿？這點時間就學了這麼多嗎。看來學習的密度相當高呢。」

雷肯擁有原本世界的藥草知識。因此，了解關於藥草的效果與配合的基本見解。不過，如果拜了普通的藥師為師的話，接觸的藥草種類恐怕會少很多吧。

「那麼，今天是出診日。接下來要巡視患者的家。兩位有帶便當嗎？」
「有攜帶些食物。艾達。也有你的份。」
「感謝。」
「很好。那麼首先，要把手洗乾淨。水井旁有放著去汙粉，要好好擦在手上來清洗。不只是手指，手肘以下全都要洗。然後，有沒有乾淨的擦手布？」