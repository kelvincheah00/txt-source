# novel

- title: 狼は眠らない
- title_zh: 狼不會入眠
- author: 支援BIS
- illust:
- source: http://ncode.syosetu.com/n3930eh/
- cover: https://web-ace.jp/rs/ace/youngaceup/contents/1000116/YAUP_ookami_nenai_main.jpg
- publisher: syosetu
- date: 2019-12-31T10:24:00+08:00
- status: 連載
- novel_status: 0x0300

## sources

- https://ncode.syosetu.com/n3930eh/
- https://ncode.syosetu.com/n7130fe/

## illusts

- 新川権兵衛
- 田ヶ喜一
- 

## publishers

- syosetu

## series

- name: 狼は眠らない

## preface


```
無論在哪個世界，都不會迷失自己應該前進的道路。 只是真摯地追求強大——冒險者雷肯的英雄譚，開幕！

異世界に落ちた腕利き冒険者〈片目狼〉レカンは、さらなる強さを目指して迷宮に潜る。いつしか彼は最強の冒険者と呼ばれるようになってゆき、富と名声、仲間と居場所を手に入れる。これは自由に生きた男レカンの冒険譚である。
```

## tags

- node-novel
- syosetu
- ゴーレム
- サイエンティスト
- スキル
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- マッド
- 伝説
- 出会い
- 女施療師
- 女騎士
- 旅
- 暗殺
- 残酷な描写あり
- 神官
- 神殿
- 薬師
- 貴族
- 迷宮
- 陰謀
- 魔法
- 魔法剣
- 魔獣

# contribute

- MrNCT
- RuneMagician
- 灵梦与魔理莎
- 

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n3930eh

## esjzone

- novel_id: 1563843171

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n3930eh&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n3930eh/)
- dm5漫畫 http://www.dm5.com/manhua-langbuhuirumian/
- [狼不会入眠吧](https://tieba.baidu.com/f?kw=%E7%8B%BC%E4%B8%8D%E4%BC%9A%E5%85%A5%E7%9C%A0&ie=utf-8 "")
- 

